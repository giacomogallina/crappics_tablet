import numpy as np
import cv2
# import time
import pyautogui

pyautogui.PAUSE = 0
pyautogui.FAILSAFE = False


def try_get_green_pos(cap):
    # print("hey")
    # Capture frame-by-frame
    ret, frame = cap.read()
    # print("hey")
    cv2.imshow("frame", frame)
    hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    cv2.imshow("hsv frame", hsv_frame)
    # print("hey")
    # green_mask = cv2.inRange(hsv_frame, (45, 200, 200), (70, 255, 255))
    green_mask = cv2.inRange(hsv_frame, (30, 25, 220), (70, 150, 256))
    cv2.imshow("green mask", green_mask)
    green_mask_2 = cv2.GaussianBlur(green_mask, (5, 5), 0)
    # print("hey")
    _, green_mask_2 = cv2.threshold(green_mask_2, 96, 255, cv2.THRESH_BINARY)
    cv2.imshow("green mask 2", green_mask_2)
    mom = cv2.moments(green_mask_2)
    if mom["m00"] > 1:
        s = mom["m00"]
        x = mom["m10"] / s
        y = mom["m01"] / s
        dx2 = mom["m20"] / s - x**2
        dy2 = mom["m02"] / s - y**2
        pos = (x, y, dx2 + dy2)
        return pos


def wait_for_green_pos(cap):
    pos = try_get_green_pos(cap)
    while pos is None:
        pos = try_get_green_pos(cap)
    return pos


input_points = []
for i in range(4):
    a = input("press enter to register corner")
    cap = cv2.VideoCapture(0)
    x, y, d2 = wait_for_green_pos(cap)
    input_points.append((x, y))
    cap.release()
    print("corner registered!")

input_points = np.float32(input_points)

w, h = pyautogui.size()
output_points = np.float32([(0, 0), (0, h-1), (w-1, h-1), (w-1, 0)])

M = cv2.getPerspectiveTransform(input_points, output_points)

print(input_points)
print(output_points)
print(M)


def project(pos, M):
    x, y, d2 = pos
    x1t, y1t, t = M.dot(np.float32([x, y, 1]))
    return (x1t/t, y1t/t)


cap = cv2.VideoCapture(0)
mouse_pressed = False
while(True):
    pos = try_get_green_pos(cap)
    if pos is not None:
        print(pos)
        _, _, d2 = pos
        # print(project(pos, M))
        # print()
        x, y = project(pos, M)
        # print("ok")
        if 0 <= x < w and 0 <= y < h:
            # print("ok")
            # pass
            if d2 > 100 and mouse_pressed:
                pyautogui.mouseUp()
                mouse_pressed = False
            elif d2 <= 80 and not mouse_pressed:
                pyautogui.mouseDown()
                mouse_pressed = True
            pyautogui.moveTo(int(x), int(y))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
